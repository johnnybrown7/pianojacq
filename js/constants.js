
// Constant definitions for the music trainer in JavaScript
// (C) Jacques Mattheij 2020; jacques@modularcompany.com
// All rights reserved, 

// modes

const listening    = 1;  // playing the music without interaction with the user
const following    = 2;  // user indicates the tempo; software will wait until the user catches up
const automatic    = 3;	 // the range slider is operated by the computer
const playalong    = 4;  // music indicates the tempo; user can play and is graded according to how well they play compared to the score

// we use this to indicate which staff we are working for

const bass = 0;
const treble = 1;

// states

const paused = 1;       // the user has commanded a pause or the program has not started yet
const waiting = 2;      // we are waiting for the user to play notes
const running = 3;      // the animation is scrolling, any user played notes are ahead of where they should be
const finished = 4;     // at the end of a practice 

// hands

const lefthand = 1;
const righthand = 2;
const bothhands = 3;

// This is the number of history entries for every tick that we
// have for each score. The history is used to record the users'
// performance on a particular piece. Making this longer will
// require an updated user interface.

const stat_history = 10;

